import Game from "./game.mjs";

const addRoomBtn = document.getElementById("add-room-btn");
const game = new Game();


addRoomBtn.addEventListener("click", () => game.createRoom());
