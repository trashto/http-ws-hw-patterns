import * as config from "./config";
import { jokes } from "../data";

class CommentFactory {

  createComment(type, data) {
    switch (type) {
      case "greeting":
        return new Greeting(data);
      case "result":
        return new Result(data)
      case "start":
        return new Start(config.SECONDS_FOR_GAME)
      case "almost":
        return new Almost(data.username, data.progress)
      case "jokes":
        return new Jokes(jokes)
      case "finish":
        return new Finish(data)

      default:
        break;
    }
  }


}

class Greeting {
  constructor(name) {
    this.name = name
  }
  get() {
    return `Привет ${this.name}!!! \n Рады приветствовать тебя на клавогонках =)`
  }
}

class Start {
  constructor(gameTime) {
    this.gameTime = gameTime
  }
  get() {
    return `Итак мы начинаем, сегодня я, представитель 
    федерации клавогонок, буду коментировать ваш заезд.\n На 
    всё про всё у вас ${this.gameTime} секунд.\n Всем удачи!!!`
  }
}

class Finish {
  constructor(name) {
    this.name = name;
  }
  get() {
    return `Водила ${this.name} пересекает финишную черту боком! Вот это зрелище!`
  }
}
class Result {
  constructor(finishers) {
    this.finishers = finishers;
  }
  get() {
    let message = "Гонка окончена.\nРезультаты: \n";

    for (let i = 0; i < 3; i++) {
      const finisher = this.finishers[i]

      if (finisher) {
        message += `${i + 1}# ${finisher}\n`;
      }
    }

    if (!this.finishers) {
      message += "- хуже чем мои оценки за домашку :("
    }
    return message;
  }
}
class Almost {
  constructor(name, progress) {
    this.name = name;
    this.progress = progress;
  }
  get() {
    return `Гонщик ${this.name} уже почти у финиша!\nОн проехал целых ${this.progress}% пути.`
  }
}

class Jokes {
  constructor(jokes) {
    this.jokes = jokes
  }
  get() {
    const randomIndex = Math.floor(Math.random() * this.jokes.length)
    return this.jokes[randomIndex]
  }
}
export default CommentFactory