import * as config from "./config";
import { texts } from "../data"
import CommentFactory from "./commentsFactory";

const totalTime = config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME;

const getRandomTextId = () => {
  const total = texts.length;
  return Math.floor(Math.random() * total)
}

class Game {
  constructor(io) {
    this.io = io;
    this.users = {};
    this.rooms = [];
    this.commentFactory = new CommentFactory();
  }

  sendComment(text, room) {
    this.io.to(room.name).emit("COMMENT", text);
  }

  createUser(socket) {
    const username = socket.handshake.query.username;
    if (this.users[username]) {
      socket.emit("ERROR_NOT_UNIQUE");
      socket.disconnect();
      return;
    }
    this.users[username] = socket;
    socket.on("disconnect", () => {
      this.deleteUser(username);
    })
    this.sendRooms(username);
    return username;
  }

  joinRoom(username, roomname) {
    const room = this.rooms.find(room => room.name === roomname);
    room.users.push({ username: username, ready: false });

    this.users[username].emit("SHOW_GAMEPAGE", roomname);

    this.users[username].join(roomname);
    this.io.to(roomname).emit("SHOW_USERS", room.users);
    this.sendRoomsAllUsers();
    const comment = this.commentFactory.createComment("greeting", username);
    this.sendComment(comment.get(), room);
  }

  sendRooms(username) {
    const rooms = this.rooms
      .filter(room => !room.isGameStarted)
      .filter(room => room.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM)
      .map(room => {
        return { name: room.name, players: room.users.length }
      })

    this.users[username].emit("SHOW_ROOM", rooms);


  }

  sendRoomsAllUsers() {
    const usernames = Object.keys(this.users);
    for (const username of usernames) {
      this.sendRooms(username)
    }
  }

  createRoom(username, roomname) {
    if (this.rooms.some(room => room.name === roomname)) {
      this.users[username].emit("USER_ERROR", "Room name not unique");
      return;
    }
    this.rooms.push({ name: roomname, isGameStarted: false, timerId: null, users: [], finishers: [] })
    this.sendRoomsAllUsers();
    this.joinRoom(username, roomname);
  }

  userReady(username) {
    const room = this.findRoomByUserName(username);
    const user = room.users.find(user => user.username === username);
    user.ready = !user.ready;
    this.io.to(room.name).emit("USER_STATUS", user);

    if (this.isEveryoneReady(room)) {
      this.startGame(room)
    }
  }

  isEveryoneReady(room) {
    return room.users.length > 0 && room.users.every(user => user.ready)
  }

  startGame(room) {
    room.isGameStarted = true;
    this.sendRoomsAllUsers();
    room.timerId = setTimeout(() => {
      this.finishGame(room);
    }, totalTime * 1000)

    room.jokesTimerId = setInterval(() => {
      const comment = this.commentFactory.createComment("jokes");
      this.sendComment(comment.get(), room);

    }, 10000)

    this.io.to(room.name).emit("GAME_STARTED", {
      textId: getRandomTextId(),
      waitTimer: config.SECONDS_TIMER_BEFORE_START_GAME,
      gameTimer: config.SECONDS_FOR_GAME
    })
    room.users.every(user => user.ready = false);
    const comment = this.commentFactory.createComment("start");
    this.sendComment(comment.get(), room);
  }

  userProgress(username, progress) {
    const room = this.findRoomByUserName(username);

    this.io.to(room.name).emit("PROGRESS_UPDATE", { username, progress })
    if (progress > 60) {
      const comment = this.commentFactory.createComment("almost", { username, progress });
      this.sendComment(comment.get(), room);
    }
    if (progress >= 100) {

      const comment = this.commentFactory.createComment("finish", username);
      this.sendComment(comment.get(), room);

      room.finishers.push(username);

      if (this.isEveryoneFinished(room)) {
        this.finishGame(room);
      }

    }
  }

  isEveryoneFinished(room) {
    return room.users.length > 0 && room.users.every(user => room.finishers.includes(user.username))
  }

  finishGame(room) {
    const comment = this.commentFactory.createComment("result", room.finishers);
    this.sendComment(comment.get(), room);
    room.users.every(user => user.ready = false);
    this.io.to(room.name).emit("GAME_OVER", room.finishers).emit("SHOW_USERS", room.users)
    room.finishers = [];
    room.isGameStarted = false;
    this.sendRoomsAllUsers()
    if (room.timerId) {
      clearTimeout(room.timerId);
    }
    if(room.jokesTimerId){
      clearInterval(room.jokesTimerId);
    }
  }

  findRoomByUserName(username) {
    for (const room of this.rooms) {
      for (const user of room.users) {
        if (user.username === username) {
          return room;
        }
      }
    }
  }


  leaveRoom(username) {
    const room = this.findRoomByUserName(username);

    if (!room) {
      return;
    }

    this.users[username].leave(room.name);

    this.deleteUserFromRoom(room, username);

    if (room.users.length <= 0) {
      this.deleteRoom(room);
      return;
    }
    this.io.to(room.name).emit("SHOW_USERS", room.users);

    if (this.isEveryoneReady(room)) {
      this.startGame(room)
    }
    if (this.isEveryoneFinished(room)) {
      this.finishGame(room);
    }

    this.sendRoomsAllUsers();
  }

  deleteRoom(room) {
    const index = this.rooms.indexOf(room);
    this.rooms.splice(index, 1);
    this.sendRoomsAllUsers();
  }

  deleteUserFromRoom(room, username) {
    const user = room.users.find(user => user.username === username);
    const index = room.users.indexOf(user);
    room.users.splice(index, 1);
  }

  deleteUser(username) {
    this.leaveRoom(username)
    delete this.users[username]
  }

}




export default io => {
  const game = new Game(io)

  io.on("connection", socket => {
    const username = game.createUser(socket);

    socket.on("ADD_ROOM", (roomname) => {
      game.createRoom(username, roomname)
    });
    socket.on("JOIN_ROOM", ({ roomname, username }) => {
      game.joinRoom(username, roomname)
    });
    socket.on("USER_LEFT_ROOM", () => {
      game.leaveRoom(username)
    });
    socket.on("USER_READY", () => {
      game.userReady(username);
    });
    socket.on("USER_PROGRESS_CHANGED", (progress) => {
      game.userProgress(username, progress)
    });

  });
};


